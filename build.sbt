name := """play-bluemkt"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.6"

// http://play-bootstrap3.herokuapp.com/
//resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "junit" % "junit" % "4.11",
  "mysql" % "mysql-connector-java" % "5.1.36",
  "au.com.bytecode" % "opencsv" % "2.4"
)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

// BlueMKT Configuration after play-java template generated
play.PlayImport.PlayKeys.playDefaultPort := 9090
libraryDependencies += evolutions