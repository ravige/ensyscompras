package models;

import com.avaje.ebean.ExpressionList;
import models.bluemkt.Account;
import models.bluemkt.Contact;
import org.junit.*;

import static org.junit.Assert.*;

import play.test.WithApplication;

import static play.test.Helpers.*;

/**
 * Created by Adrian on 07/08/2015.
 */
public class ModelTest extends WithApplication {
    Account commonAccount;
    @Before
    public void setUp() { start(fakeApplication(inMemoryDatabase())); }

    /**
     * check that we can insert a row, and retrieve it again
     */
    @Test
    public void createAndRetrieveAccount() {
        new Account("ETN", "ABCDF", "12345", "http://etn.com.mx").save();
        Account commonAccount = Account.find.where().eq("name", "ETN").findUnique();
        assertNotNull(commonAccount);
        assertEquals("ETN", commonAccount.name);
    }

    @Test
    public void createContactForAccount() {
        new Contact(commonAccount, "Hazael", "Vallecillos", "promociones@etn.com.mx", "cambiarme").save();
        new Contact(commonAccount, "Hazael", "Del Sagrado Corazon", "esteesotro@etn.com.mx", "nocambiarme").save();
        Contact hazael = Contact.find.where().eq("email", "promociones@etn.com.mx").findUnique();
        ExpressionList contactsETN = Contact.find.where().like("email", "%etn.com.mx");
        assertNotNull(hazael);
        assertEquals("Kbiarme", hazael.getPassword());
        assertEquals(contactsETN.findRowCount(), 2);
    }
}
