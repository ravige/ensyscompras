/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.ensys.models;

import com.avaje.ebean.Model;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author joel
 */
@Entity
@Table(name = "unitCost")
@XmlRootElement
public class UnitCost extends Model{
    
    
        @Id
    @Basic(optional = false)
    @Column(name = "ID", length = 36)
    public String id;

        
        
        
  
    public UnitCost(String id) {
        id = UUID.randomUUID().toString();
    }

        public static final Finder<String, UnitCost> find = new Finder<>(UnitCost.class);  
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnitCost other = (UnitCost) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UnitCost{" + "id=" + id + '}';
    }
    
        
}
