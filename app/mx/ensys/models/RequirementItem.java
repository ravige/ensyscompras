/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.ensys.models;

import com.avaje.ebean.Model;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author joel
 */
@Entity
@Table(name = "requirementItem")
@XmlRootElement
public class RequirementItem extends Model {
    
        @Id
    @Basic(optional = false)
    @Column(name = "ID", length = 36)
    public String id;
    @Column(name = "description")
    public String description;
     @Column(name = "short_description")
    public String shortDescription; 
       @Column(name = "status")
    public String status;    
     @Column(name = "quantityt")
    public int quantity;    
     @Column(name = "coin")
    public String coin; 
      @Column(name = "price")
    public int price;  
          @Column(name = "required_date")
    @Temporal(TemporalType.DATE)
 public Date requiredDate;
          
              @JoinColumn(name = "requirement", referencedColumnName = "ID", nullable = true)
    @ManyToOne
    public Requirement requirement;

    public RequirementItem() {
        id = UUID.randomUUID().toString();
    }
    
                  public static final Finder<String, RequirementItem> find = new Finder<>(RequirementItem.class);
          
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(Date requiredDate) {
        this.requiredDate = requiredDate;
    }

    public Requirement getRequirement() {
        return requirement;
    }

    public void setRequirement(Requirement requirement) {
        this.requirement = requirement;
    }

    public static Finder<String, RequirementItem> getFind() {
        return find;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RequirementItem other = (RequirementItem) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

   

    @Override
    public String toString() {
        return "RequirementItem{" + "id=" + id + ", description=" + description + '}';
    }
    

        
}
