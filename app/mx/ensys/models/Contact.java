/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.ensys.models;


import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import play.Logger;
import play.data.validation.Constraints;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Jesa
 * @author Adrian - Adapted for Play Java
 * @version 2.0
 */
@Entity
@Table(name = "contact")
@XmlRootElement
/** fixme: Do not forget to execute this:
 para que la contraseña sea sensible a mayus y minus
 ALTER TABLE contact MODIFY COLUMN BM_PASSWORD_C VARCHAR (255) BINARY CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL;
 */
public class Contact extends Model {
    // We don't need BIG columns length (255)! - The required are enough = UUID = 35 including dashes '-'
    // https://serversforhackers.com/mysql-utf8-and-indexing
    // http://www.arsys.info/programacion/myisam-o-innodb-elige-tu-motor-de-almacenamiento-mysql/
    @Id
    @Basic(optional = false)
    @Column(name = "ID", length = 36)
    public String id;
    @Column(name = "ISDELETED")
    public Boolean isdeleted;

    @JoinColumn(name = "ACCOUNTID", referencedColumnName = "ID", nullable = true)
    @ManyToOne
    public Account account;
    @Column(name = "LASTNAME")
    public String lastname;
    @Column(name = "FIRSTNAME")
    public String firstname;
    @Column(name = "PHONE")
    public String phone;
    @Column(name = "FAX")
    public String fax;
    @Column(name = "MOBILEPHONE")
    public String mobilephone;
    @Column(name = "HOMEPHONE")
    public String homephone;
    @Column(name = "OTHERPHONE")
    public String otherphone;
    @Column(name = "EMAIL", unique = true, nullable = false, length = 190)
    @Constraints.Required
    @Constraints.Email
    public String email;
    @Column(name = "BIRTHDATE")
    @Temporal(TemporalType.DATE)
    public Date birthday;
    @Column(name = "DESCRIPTION")
    public String description;
    @Column(name = "OWNERID")
    public String ownerid;

    @Column(name = "CREATEDBYID")
    public String createdbyid;

    @Column(name = "LASTMODIFIEDBYID")
    public String lastmodifiedbyid;
    @Column(name = "LASTACTIVITYDATE")
    @Temporal(TemporalType.DATE)
    public Date lastactivitydate;
    @Column(name = "EMAILBOUNCEDREASON")
    public String emailbouncedreason;
    @Column(name = "EMAILBOUNCEDDATE")
    public String emailbounceddate;
    @Column(name = "ISEMAILBOUNCED")
    public Boolean isemailbounced;
    @Column(name = "PHOTOURL")
    public String photourl;
    @Column(name = "AGE")
    public Integer age;
    @Column(name = "BM_PASSWORD", length = 64, nullable = false)
    private byte[] bmPassword;
    @Column(name = "EXTERNAL_ID")
    public String externalId;
    @Column(name = "GENDER")
    public String gender;
    @Column(name = "LANGUAGES")
    public String languages;
    @Column(name = "PROFILE_PICTURE")
    public String picture;
    @Column(name = "BMROLE")
    public String bmrole;


    public Contact() {
        id = UUID.randomUUID().toString();
    }

    public Contact(Account account, String firstname, String lastname, String email, String bmPassword) {
        id = UUID.randomUUID().toString();
        this.account = account;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email.toLowerCase();
        this.bmPassword = getSha512(bmPassword);
        Logger.debug(this.toString());
    }

    public Contact(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Account getAccount() {
        return account;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getPhone() {
        return phone;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public String getEmail() {
        return email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Integer getAge() {
        return age;
    }

    public String getPhotourl() {
        return photourl;
    }

    public String getGender() {
        return gender;
    }

    public String getPicture() {
        return picture;
    }

    public String getBmrole() {
        return bmrole;
    }

    public String getPassword() {
        return bmPassword.toString();
    }

    public void setPassword(String password) {
        this.bmPassword = getSha512(password);
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase();
    }


    
    

    public static final Finder<String, Contact> find = new Finder<>(Contact.class);

    public static Contact findByEmailAndPassword(String email, String password) {
        return find
                .where()
                .eq("email", email.toLowerCase())
                .eq("BM_PASSWORD", getSha512(password))
                .findUnique();
    }

    public static Contact findByEmail(String email) {
        return find
                .where()
                .eq("email", email.toLowerCase())
                .findUnique();
    }

    public static byte[] getSha512(String value) {
        try {
            return MessageDigest.getInstance("SHA-512").digest(value.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contact)) {
            return false;
        }
        Contact other = (Contact) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "models.bluemkt.Contact[ id=".concat(id).concat(" name:").concat(firstname).concat(" ").concat(lastname).concat(" ]");
    }


}
