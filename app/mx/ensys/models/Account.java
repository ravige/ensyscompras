/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.ensys.models;


import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import java.util.ArrayList;
import play.Logger;

import java.util.List;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Jesa
 * @author Adrian - Modified and addapted to Play Java
 * @version 2.0 - Lets call Play version version 2.0 :-)
 */
@NamedQueries({
        @NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a"),
        @NamedQuery(name = "Account.findById", query = "SELECT a FROM Account a WHERE a.id = :id"),
        @NamedQuery(name = "Account.findByIsdeleted", query = "SELECT a FROM Account a WHERE a.isdeleted = :isdeleted"),
        @NamedQuery(name = "Account.findByName", query = "SELECT a FROM Account a WHERE a.name = :name"),
        @NamedQuery(name = "Account.findByType", query = "SELECT a FROM Account a WHERE a.type = :type"),
        @NamedQuery(name = "Account.findByParentid", query = "SELECT a FROM Account a WHERE a.parentid = :parentid"),
        @NamedQuery(name = "Account.findByPhone", query = "SELECT a FROM Account a WHERE a.phone = :phone"),
        @NamedQuery(name = "Account.findByWebsite", query = "SELECT a FROM Account a WHERE a.website = :website"),
        @NamedQuery(name = "Account.findByIndustry", query = "SELECT a FROM Account a WHERE a.industry = :industry"),
        @NamedQuery(name = "Account.findByOwnerid", query = "SELECT a FROM Account a WHERE a.ownerid = :ownerid"),
        @NamedQuery(name = "Account.findByCreateddate", query = "SELECT a FROM Account a WHERE a.createddate = :createddate"),
        @NamedQuery(name = "Account.findByCreatedbyid", query = "SELECT a FROM Account a WHERE a.createdbyid = :createdbyid"),
        @NamedQuery(name = "Account.findByLastmodifieddate", query = "SELECT a FROM Account a WHERE a.lastmodifieddate = :lastmodifieddate"),
        @NamedQuery(name = "Account.findByLastmodifiedbyid", query = "SELECT a FROM Account a WHERE a.lastmodifiedbyid = :lastmodifiedbyid"),
        @NamedQuery(name = "Account.findBySystemmodstamp", query = "SELECT a FROM Account a WHERE a.systemmodstamp = :systemmodstamp"),
        @NamedQuery(name = "Account.findByLastactivitydate", query = "SELECT a FROM Account a WHERE a.lastactivitydate = :lastactivitydate"),
        @NamedQuery(name = "Account.findByLastvieweddate", query = "SELECT a FROM Account a WHERE a.lastvieweddate = :lastvieweddate"),
        @NamedQuery(name = "Account.findByLastreferenceddate", query = "SELECT a FROM Account a WHERE a.lastreferenceddate = :lastreferenceddate"),
        @NamedQuery(name = "Account.findByAccountsource", query = "SELECT a FROM Account a WHERE a.accountsource = :accountsource"),
        @NamedQuery(name = "Account.findByActiveC", query = "SELECT a FROM Account a WHERE a.activeC = :activeC"),
        @NamedQuery(name = "Account.findByLandingPageC", query = "SELECT a FROM Account a WHERE a.landingPageC = :landingPageC"),
        @NamedQuery(name = "Account.findByCustomerpriorityC", query = "SELECT a FROM Account a WHERE a.customerpriorityC = :customerpriorityC"),
        //Personalized queries
        @NamedQuery(name = "Account.findByIdAndParentId", query = "SELECT a FROM Account a WHERE a.id = :id OR a.parentid.id = :parentId")})
@Entity
@Table(name = "account")
@XmlRootElement
public class Account extends Model {
    @Id
    @Basic(optional = false)
    @Column(name = "ID", length = 36)
    public String id;
//    /** A contact is a known user */
//    @OneToMany(mappedBy = "account")
//    public List<Contact> contactList = new ArrayList<>();


    @Basic(optional = false)
    @Column(name = "NAME", nullable = false)
    public String name;
    @Column(name = "TYPE")
    public String type;
    @JoinColumn(name = "PARENTID", referencedColumnName = "ID")
    @ManyToOne
    public Account parentid;
    @Column(name = "PHONE")
    public String phone;
    @Column(name = "WEBSITE")
    public String website;
    @Column(name = "PHOTOURL")
    public String photourl;
    // Removed ENUM because is unknown for Play InMemoryDatabase
    // [error] - play.api.db.evolutions.DefaultEvolutionsApi - Tipo de dato desconocido : "ENUM"
    @Column(name = "INDUSTRY", length = 25)
//, columnDefinition="enum('Retail','Shipping','Hospitality','Healthcare','Government','Transportation','Food and Beverage')")
    public String industry;
    @Column(name = "DESCRIPTION")
    public String description;
    @Column(name = "OWNERID")
    public String ownerid;

    

    //Added privacyPolicy by Jose on 2015 01 13 17:37 pm
    @Column(name = "PRIVACYPOLICY")
    public String privacyPolicy;


    private static Account BLUEMKT_TRIAL_ACCOUNT;
    public final static String TRIAL_ACCOUNT_ID = "00000000-0000-0000-0000-0000000000FF";
    //private final static String MC_API_KEY = "c5b4ab8fd1da2878e4c86d6319ff744d-us9";

    /**
     * A final method cannot be overridden in a subclass, so lets assign a unique account for Trial users
     *
     * @return Account for demo trials
     */
    public final static Account getTrialAccount() {
        if (BLUEMKT_TRIAL_ACCOUNT == null) {
            Account trialAccount = find.byId(TRIAL_ACCOUNT_ID);
            if (trialAccount == null) {
                trialAccount = new Account(TRIAL_ACCOUNT_ID);
                trialAccount.name = "BlueMKT Trial Account";
                trialAccount.website = "http://bluemkt.mx";

                trialAccount.save();
            }
            BLUEMKT_TRIAL_ACCOUNT = trialAccount;
        }
        return BLUEMKT_TRIAL_ACCOUNT;
    }

    private Account(String id) {
        this.id = id;
    }

    public Account() {
        this.id = UUID.randomUUID().toString();
    }

    public Account(String name, String mailchimpapikey, String mailchimplistidC, String website) {
        this.id = UUID.randomUUID().toString();
        this.name = name;

        this.website = website;
        Logger.debug(this.toString());
    }

    /*****************************************************************
     /* Getters and Setters are automatically generated by Scala! :-) * /
     /* BUT a BAD practice---- considere to revert --- */
    public String getId() {
        return id;
    }

   

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Account getParentid() {
        return parentid;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    public String getPhotourl() {
        return photourl;
    }

   

    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

//    public List<Contact> getContactList() {
//        return contactList;
//    }
//
//    public void setContactList(List<Contact> contactList) {
//        this.contactList = contactList;
//    }

    
    
    /**
     * @return The EBean ORM finder method.
     */
    public static Finder<String, Account> find() {
        return new Finder<>(Account.class);
    }

    public static final Finder<String, Account> find = new Finder<>(Account.class);

    /**
     * Find sub-accounts of specific account
     * @param parentId
     * @return List of sub-account IDs
     */
    public static List<Object> findIdsByParentId(Account parentId) {
        // SELECT a FROM Account a WHERE a.parentid = :parentid
        // SELECT id FROM Account a WHERE a.parentid = :parentid
        return find.where().eq("parentid", parentId).findIds();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return name.concat(" ").concat(id);
    }

}
