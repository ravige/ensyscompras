/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.ensys.models;


import com.avaje.ebean.Model;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Jesa
 * @author Adrian - Adapted for Play Java
 * @version 2.0
 */
@Entity
@Table(name = "requirement")
@XmlRootElement

public class Requirement extends Model {
    // We don't need BIG columns length (255)! - The required are enough = UUID = 35 including dashes '-'
    // https://serversforhackers.com/mysql-utf8-and-indexing
    // http://www.arsys.info/programacion/myisam-o-innodb-elige-tu-motor-de-almacenamiento-mysql/
    @Id
    @Basic(optional = false)
    @Column(name = "ID", length = 36)
    public String id;

    

    @JoinColumn(name = "unit_cost", referencedColumnName = "ID", nullable = true)
    @ManyToOne
    public UnitCost unitCost;
    @Column(name = "status")
    public String status;

    @Column(name = "required_date")
    @Temporal(TemporalType.DATE)

    public Date required_date;
    @JoinColumn(name = "created_by_id", referencedColumnName = "ID", nullable = true)
    @ManyToOne    
    public Contact created_by_id;

//
//      @OneToMany(mappedBy="requirement")
//  private List<RequirementItem> itemList;
    
    
    public Requirement() {
//        this.itemList = new ArrayList<>();
        id = UUID.randomUUID().toString();
    }


    public static final Finder<String, Requirement> find = new Finder<>(Requirement.class);

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public UnitCost getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(UnitCost unitCost) {
        this.unitCost = unitCost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequired_date() {
        return required_date;
    }

    public void setRequired_date(Date required_date) {
        this.required_date = required_date;
    }



    public Contact getCreated_by_id() {
        return created_by_id;
    }

    public void setCreated_by_id(Contact created_by_id) {
        this.created_by_id = created_by_id;
    }


//    public List<RequirementItem> getItemList() {
//        return itemList;
//    }
//
//    public void setItemList(List<RequirementItem> itemList) {
//        this.itemList = itemList;
//    }

    public static Finder<String, Requirement> getFind() {
        return find;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Requirement other = (Requirement) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Requirement{" + "id=" + id + ", required_date=" + required_date + '}';
    }




}
