package mx.ensys;

import play.mvc.*;

/**
 * Allows particular back-end calls only to authenticated users
 *
 * Created by Adrian Escutia on 13/08/2015.
 *
 * @author Adrian Escutia <a href="mailto:adrian.escutia-at-bluemkt.mx>AdES</a>
 *         Copyrigth 2015, ESSO Technologies | SSINCO
 */
public class Secured extends Security.Authenticator {

    @Override
    public String getUsername(Http.Context ctx) {
        return ctx.session().get("username");
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        return unauthorized();
    }
}
