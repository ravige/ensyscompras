package mx.ensys.controllers;


import com.avaje.ebean.Model;
import play.data.Form;
import play.mvc.Controller;
import mx.ensys.models.Contact;
import play.mvc.Result;

import java.util.List;

import static play.libs.Json.toJson;

/**
 * Created by Adrian Escutia on 09/08/2015.
 *
 * @author Adrian Escutia <a href="mailto:adrian.escutia-at-bluemkt.mx>AdES</a>
 *         Copyrigth 2015, ESSO Technologies | SSINCO
 */
public class ContactController extends Controller {

    /**
     * Saves an updated version of the contact data provided by user.
     * @param primaryKey The PK to the contact.
     * @return The home page.
     */
    public Result update(String primaryKey) {
        Contact contact = Contact.find.byId(primaryKey);
        contact.update(primaryKey);
        return ok(toJson(contact));
    }

    public Result save() {
        Contact contactModel = Form.form(Contact.class).bindFromRequest().get();
        contactModel.save();
        return ok(toJson(contactModel));
    }

    /**
     * Deletes the contact.
     * @param primaryKey The PK of the contact to be deleted.
     * @return The deleted contact.
     */
    public Result delete(String primaryKey) {
        Contact contactModel = Contact.find.byId(primaryKey);
        contactModel.delete();
        return ok(toJson(contactModel));
    }

    public Result getContact(String id) {
        Contact contact = Contact.find.byId(id);
        return ok(toJson(contact));
    }

    public Result getAllContacts() {
        List<Contact> contacts = new Model.Finder<String, Contact>(Contact.class).all();
        return ok(toJson(contacts));
    }
}
