package mx.ensys.controllers;


import com.avaje.ebean.Model;
import play.data.Form;
import play.mvc.Controller;
import mx.ensys.models.Contact;
import play.mvc.Result;

import java.util.List;
import mx.ensys.models.Requirement;
import mx.ensys.models.UnitCost;

import static play.libs.Json.toJson;

/**
 * Created by Adrian Escutia on 09/08/2015.
 *
 * @author Adrian Escutia <a href="mailto:adrian.escutia-at-bluemkt.mx>AdES</a>
 *         Copyrigth 2015, ESSO Technologies | SSINCO
 */
public class UnitCostController extends Controller {





    public Result getRequirement(String id) {
        Requirement requirement = Requirement.find.byId(id);
        return ok(toJson(requirement));
    }

    public Result getAllUnitCost() {
        List<UnitCost> unitCost = new Model.Finder<String, UnitCost>(UnitCost.class).all();
        return ok(toJson(unitCost));
    }

 

}