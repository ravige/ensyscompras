package mx.ensys.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import mx.ensys.models.Account;
import mx.ensys.models.Contact;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints;
import play.libs.Json;
import play.mvc.*;


import java.util.List;
import mx.ensys.models.Requirement;
import static play.libs.Json.toJson;
import static play.mvc.Results.ok;

/**
 * <p>Implements the sign-up, login and logout actions
 * This class also sets some session values which are:</p>
 * <dl>
 *     <dt>username</dt><dd>The logged in user email</dd>
 *     <dt>account</dt><dd>Is the contact's (main) account; could it be parent of others or not</dd>
 *     <dt>currentAccount</dt><dd>The selected account; if a contact have multiple accounts, and on UI selects a sub-account, this is updated in currentAccount session variable</dd>
 *     <dt>accounts</dt><dd>A customer can have more than one account, this is a comma separated list of account IDs, including parent account</dd>
 * </dl>
 */
public class Application extends Controller {

    public Result save() {
        Requirement requirement = Form.form(Requirement.class).bindFromRequest().get();
        requirement.save();
        return ok(toJson(requirement));
    }
    public Result signup() {
        Form<SignUp> signUpForm = Form.form(SignUp.class).bindFromRequest();

        if (signUpForm.hasErrors()) {
            return badRequest(signUpForm.errorsAsJson());
        }
        SignUp newUser = signUpForm.get();
        Contact existingUser = Contact.findByEmail(newUser.email);
        if (existingUser != null) {
            return badRequest(buildJsonResponse("error", "User exists"));
        } else {
            // Contact is synonym of user, allowing users to create accounts
            // ... then, with emailing follow up to activate an account and pay for it :-)
            String randomTS = "" + Math.floor(System.currentTimeMillis() / 60000); // Minutes
            Contact contactUser =
                    new Contact(Account.getTrialAccount(),
                            "Demo User " + randomTS,
                            "Lastname " + randomTS,
                            newUser.email,
                            newUser.password);
            contactUser.save();
            session().clear();
            session("username", newUser.email);

            return ok(buildJsonResponse("success", "User created successfully"));
        }
    }

    public static class UserForm {
        @Constraints.Required
        @Constraints.Email
        public String email;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public static class SignUp extends UserForm {
        @Constraints.Required
        @Constraints.MinLength(6)
        public String password;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    private static ObjectNode buildJsonResponse(String type, String message) {
        ObjectNode wrapper = Json.newObject();
        ObjectNode msg = Json.newObject();
        msg.put("message", message);
        wrapper.put(type, msg);
        return wrapper;
    }

    /**
     * Validates if user is registered, if so, session variables are set:
     * <dl>
     *     <dt>username</dt><dd>The logged in user email</dd>
     *     <dt>account</dt><dd>Is the contact's (main) account; could it be parent of others or not</dd>
     *     <dt>currentAccount</dt><dd>The selected account; if a contact have multiple accounts, and on UI selects a sub-account, this is updated in currentAccount session variable</dd>
     *     <dt>accounts</dt><dd>A customer can have more than one account, this is a comma separated list of account IDs, including parent account</dd>
     * </dl>
     * @return
     * @see https://www.playframework.com/documentation/2.0/JavaSessionFlash
     */
    public Result login() {
        Form<Login> loginForm = Form.form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(loginForm.errorsAsJson());
        }
        Login loggingInUser = loginForm.get();
        Contact user = Contact.findByEmailAndPassword(loggingInUser.email, loggingInUser.password);
        if(user == null) {
            return badRequest(buildJsonResponse("error", "Incorrect email or password"));
        } else {
            session().clear();
            session("username", loggingInUser.email);
            String accountId = user.getAccount().getId();
            // Inserted twice... if a "super user", then has access to several accounts throw parent
            // account is the account he has rights, currentAccount is specific account he is analyzing in dashboard
            // said that, currentAccount could change, but not account
            session("account", accountId);
            session("currentAccount", accountId);
            //<editor-fold desc="Sub-accounts and parent Account">
            List<Object> accountIds = Account.findIdsByParentId(user.getAccount());
            // fixme - what if no sub-account found? Empty List or null? Would throw exception!?  -- VALIDATE
            Logger.debug("Sub-Accounts {0}", accountIds);
            // adding also the parent ID to the list
            accountIds.add(accountId);
            String arrayAsString = accountIds.toString();
            // remove [] from string; a List.toString() returns something like: "[val1, val2]"
            session("accounts", arrayAsString.substring(1,arrayAsString.length()-1).trim());
            // todo - Test Accounts with no parent, with one and two sub-accounts
            //</editor-fold>

            ObjectNode wrapper = Json.newObject();
            ObjectNode msg = Json.newObject();
            msg.put("message", "Logged in successfully");
            msg.put("email", loggingInUser.email);
            msg.put("user", toJson(user));
            wrapper.put("success", msg);
            return ok(wrapper);
        }
    }

    public Result logout() {
        session().clear();
        System.out.println("clear");
        return ok(buildJsonResponse("success", "Logged out successfully"));
    }

    public Result isAuthenticated() {
        if(session().get("username") == null) {
            return unauthorized();
        } else {
            ObjectNode wrapper = Json.newObject();
            ObjectNode msg = Json.newObject();
            msg.put("message", "User is logged in already");
            msg.put("user", session().get("username"));
            wrapper.put("success", msg);
            return ok(wrapper);
        }
    }

    public static class Login extends SignUp {
        // same fields... but extending just in case other behavior could be required
    }

}
