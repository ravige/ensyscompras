package mx.ensys.controllers;


import com.avaje.ebean.Model;
import play.data.Form;
import play.mvc.Controller;
import mx.ensys.models.Contact;
import play.mvc.Result;

import java.util.List;
import mx.ensys.models.Requirement;
import mx.ensys.models.UnitCost;

import static play.libs.Json.toJson;

/**
 * Created by Adrian Escutia on 09/08/2015.
 *
 * @author Adrian Escutia <a href="mailto:adrian.escutia-at-bluemkt.mx>AdES</a>
 *         Copyrigth 2015, ESSO Technologies | SSINCO
 */
public class RequirementController extends Controller {

    /**
     * Saves an updated version of the contact data provided by user.
     * @param primaryKey The PK to the contact.
     * @return The home page.
     */
    public Result update(String primaryKey) {
        
        Requirement requirement = Requirement.find.byId(primaryKey);
        
        requirement.update(primaryKey);
        return ok(toJson(requirement));
    }

    public Result save() {
        Requirement requirement = Form.form(Requirement.class).bindFromRequest().get();
        System.out.println("res "+requirement);
//        UnitCost unitCost = UnitCost.find.byId(requirement.u);
        requirement.save();
        return ok(toJson(requirement));
    }

    /**
     * Deletes the contact.
     * @param primaryKey The PK of the contact to be deleted.
     * @return The deleted contact.
     */
    public Result delete(String primaryKey) {
        Requirement requirement = Requirement.find.byId(primaryKey);
        requirement.delete();
        return ok(toJson(requirement));
    }

    public Result getRequirement(String id) {
        Requirement requirement = Requirement.find.byId(id);
        return ok(toJson(requirement));
    }

    public Result getAllRequirements() {
        List<Requirement> requirements = new Model.Finder<String, Requirement>(Requirement.class).all();
        
        return ok(toJson(requirements));
    }

 

}