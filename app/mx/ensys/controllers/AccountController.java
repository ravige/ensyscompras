package mx.ensys.controllers;

import com.avaje.ebean.Model;
import mx.ensys.models.Account;
import play.data.Form;
import play.mvc.*;


import java.util.List;

import static play.libs.Json.toJson;

/**
 * Created by Adrian Escutia on 09/08/2015.
 *
 * @author Adrian Escutia <a href="mailto:adrian.escutia-at-bluemkt.mx>AdES</a>
 *         Copyrigth 2015, ESSO Technologies | SSINCO
 */
public class AccountController extends Controller {
    /**
     * Saves an updated version of the warehouse data provided by user.
     * @param primaryKey The PK to the warehouse.
     * @return The home page.
     */
    public Result update(String primaryKey) {
        Account account = Account.find().byId(primaryKey);
        account.update(primaryKey);
        return ok(toJson(account));
    }

    public Result save() {
        Account accountModel = Form.form(Account.class).bindFromRequest().get();
        accountModel.save();
        return ok(toJson(accountModel));
    }

    /**
     * Deletes the account.
     * @param primaryKey The PK of the account to be deleted.
     * @return The deleted account.
     */
    public Result delete(String primaryKey) {
        Account account = Account.find().byId(primaryKey);
        account.delete();
        return ok(toJson(account));
    }

    public Result getAccount(String id) {
        Account account = Account.find.byId(id);
        return ok(toJson(account));
    }

    public Result getAllAccounts() {
        List<Account> accounts = new Model.Finder<String, Account>(Account.class).all();
        return ok(toJson(accounts));
    }
}
