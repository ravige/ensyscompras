# BlueMKT App

BlueMKT est� dividido en dos proyectos:

1. bm-client    ---> Cliente (UI), es la **`VISTA`** de la soluci�n, contiene todos los componentes de la parte visual del proyecto
    * Javascript
    >* Bower
    >* *AngularJS*
    >* Bootstrap
    * HTML
    * CSS
2. play-bluemkt ---> Backend de BlueMKT, contiene la implementaci�n de acceso a BDs y Servicios Web
    * Implementaci�n en Play! Framework.
    * EBean para acceso a base de datos - **`MODELO`**
    * **`CONTROLADORES`** -- Utilizados para proveer los servicios web y recursos (URI) para acceso a BDs, ejemplo: registro de usuarios

Para evitar cargar todos los m�dulos de Angular, Bower en GIT, ejecutar los pasos del cliente de manera local.

# Primeros pasos

## Prerequisitos y Configuraci�n (Cliente)

***OJO: la descarga de los m�dulos de Yeoman se debe ejecutar estando dentro del directorio de la App CLIENTE***

[Node.js](https://www.npmjs.com/package/npm "npm") - Instalar npm

[Yeoman](http://yeoman.io/) incluyendo sus 3 (tres) componentes: Grunt, Bower, y Yo (scaffolding tool)

    npm install -g yo bower grunt-cli angular generator-karma generator-angular

Con el comando anterior se instalan varios m�dulos en un paso, cuando se instala angular, la consola solicita si se va a instalar Bootstrap y Compass, instalar s�lo Bootstrap, NO instalar Compass.

#### Partiendo los servidores locales

Para poder intercomunicar cliente y servidor, es necesario instalar Grunt proxy, la configuraci�n ya est� hecha en el archivo del proyecto

    npm install grunt-connect-proxy --save-dev

grunt-connect-proxy

## Server
Para levantar el servidor:

Dentro del directorio de la aplicaci�n del servidor (play-bluemkt)

`./activator`

Forzando un puerto espec�fico:

`./activator "run 9090"`

La aplicaci�n ya est� preconfigurada levantar en el puerto 9090 (build.sbt)

    play.PlayImport.PlayKeys.playDefaultPort := 9090

## Client
Para correr el cliente ejecutar: `grunt serve`

Grunt levanta el servidor por defecto en el puerto 9000, se adapat� el script de Grunt para configurar un 'proxy' que redirecciona el tr�fico al backend en puerto 9090

## Instalaci�n


# Siguientes pasos

Si necesitas crear una vista, controlador o servicio de Angular, es recomendable hacerlo con Yeoman, ya que agrega al index los scripts necesarios y crea plantillas b�sicas

Ejemplos:

    yo angular:controller signup

    yo angular:view signup

    yo angular:service alerts

# Referencias / Tutoriales

[Building Modern Web Applications with AngularJS and Play Framework](http://www.toptal.com/java/building-modern-web-applications-with-angularjs-and-play-framework)

[Your First AngularJS App](http://www.toptal.com/angular-js/your-first-angularjs-app-part-2-scaffolding-building-and-testing)


# Historial y "Release notes"

## Version 0.x - MVP

* Se resumen todas las versiones y cambios implementados durante el MVP

> Salesforce como BDs

> Frontend con JSF

> Backend con EJBs

> Contenedor de aplicaciones JBoss

## BlueBeat 1.0.0-beta

* Implementaci�n basada en Radius y Coova Chilli

## BlueBeat 0.0

* Implementaci�n simple para registro de usuarios
> Errores de bloqueo en routers

# Roadmap

(Future Plans - Backlog)

## 2.0.0-beta.1

***Objetivos***

* Backend basado en Play
* Frontend basado en Angular (Yeoman, Bootstrap y Bower)
> Single Page Application [SPA](https://en.wikipedia.org/wiki/Single-page_application)
* Deploy en Heroku - Evitar la operaci�n y mantenimiento de un servidor propio para enfocarnos 100% al desarrollo
* Migraci�n y optimizaci�n de Base de Datos
> Eliminar las columnas de base de datos innecesarias migradas de Salesforce
> Eliminar vistas y tablas de BluePharma (MVP de App para m�dicos y farmacias)