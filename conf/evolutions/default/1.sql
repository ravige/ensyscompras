# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table account (
  ID                        varchar(36) not null,
  NAME                      varchar(255) not null,
  TYPE                      varchar(255),
  PARENTID                  varchar(36),
  PHONE                     varchar(255),
  WEBSITE                   varchar(255),
  PHOTOURL                  varchar(255),
  INDUSTRY                  varchar(25),
  DESCRIPTION               varchar(255),
  OWNERID                   varchar(255),
  PRIVACYPOLICY             varchar(255),
  constraint pk_account primary key (ID))
;

create table contact (
  ID                        varchar(36) not null,
  ISDELETED                 tinyint(1) default 0,
  ACCOUNTID                 varchar(36),
  LASTNAME                  varchar(255),
  FIRSTNAME                 varchar(255),
  PHONE                     varchar(255),
  FAX                       varchar(255),
  MOBILEPHONE               varchar(255),
  HOMEPHONE                 varchar(255),
  OTHERPHONE                varchar(255),
  EMAIL                     varchar(190) not null,
  BIRTHDATE                 datetime(6),
  DESCRIPTION               varchar(255),
  OWNERID                   varchar(255),
  CREATEDBYID               varchar(255),
  LASTMODIFIEDBYID          varchar(255),
  LASTACTIVITYDATE          datetime(6),
  EMAILBOUNCEDREASON        varchar(255),
  EMAILBOUNCEDDATE          varchar(255),
  ISEMAILBOUNCED            tinyint(1) default 0,
  PHOTOURL                  varchar(255),
  AGE                       integer,
  BM_PASSWORD               varbinary(64) not null,
  EXTERNAL_ID               varchar(255),
  GENDER                    varchar(255),
  LANGUAGES                 varchar(255),
  PROFILE_PICTURE           varchar(255),
  BMROLE                    varchar(255),
  constraint uq_contact_EMAIL unique (EMAIL),
  constraint pk_contact primary key (ID))
;

create table requirement (
  ID                        varchar(36) not null,
  unit_cost                 varchar(36),
  status                    varchar(255),
  required_date             datetime(6),
  created_by_id             varchar(36),
  constraint pk_requirement primary key (ID))
;

create table requirementItem (
  ID                        varchar(36) not null,
  description               varchar(255),
  short_description         varchar(255),
  status                    varchar(255),
  quantityt                 integer,
  coin                      varchar(255),
  price                     integer,
  required_date             datetime(6),
  requirement               varchar(36),
  constraint pk_requirementItem primary key (ID))
;

create table unitCost (
  ID                        varchar(36) not null,
  constraint pk_unitCost primary key (ID))
;

alter table account add constraint fk_account_parentid_1 foreign key (PARENTID) references account (ID) on delete restrict on update restrict;
create index ix_account_parentid_1 on account (PARENTID);
alter table contact add constraint fk_contact_account_2 foreign key (ACCOUNTID) references account (ID) on delete restrict on update restrict;
create index ix_contact_account_2 on contact (ACCOUNTID);
alter table requirement add constraint fk_requirement_unitCost_3 foreign key (unit_cost) references unitCost (ID) on delete restrict on update restrict;
create index ix_requirement_unitCost_3 on requirement (unit_cost);
alter table requirement add constraint fk_requirement_created_by_id_4 foreign key (created_by_id) references contact (ID) on delete restrict on update restrict;
create index ix_requirement_created_by_id_4 on requirement (created_by_id);
alter table requirementItem add constraint fk_requirementItem_requirement_5 foreign key (requirement) references requirement (ID) on delete restrict on update restrict;
create index ix_requirementItem_requirement_5 on requirementItem (requirement);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table account;

drop table contact;

drop table requirement;

drop table requirementItem;

drop table unitCost;

SET FOREIGN_KEY_CHECKS=1;

